#!/usr/bin/env bash


php bin/console doctrine:database:drop --force

php bin/console doctrine:database:create

php bin/console doctrine:schema:create

rm -rf public/uploads/images

cp -r fixturesData/images public/uploads

php bin/console doctrine:fixtures:load -n
