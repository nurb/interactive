# language: ru

@auth @fixtures
Функционал: Тестируем регистрацию и авторизацию на сайте

  @register
  Сценарий: Я пытаюсь зарегистрироваться
    Допустим я нахожусь на главной странице
    И я вижу слово "Регистрация" на странице
    И я кликаю по ссылке с id "app_register"
    Тогда я заполняю поля данными
      | fos_user_registration_form_email                | test@gmail.com        |
      | fos_user_registration_form_phoneNumber          | 0555 44 44 44         |
      | fos_user_registration_form_plainPassword_second | test123!@#            |
      | fos_user_registration_form_plainPassword_first  | test123!@#            |
      | fos_user_registration_form_fullName             | Ivanov Ivan Ivanovich |
      | fos_user_registration_form_organizationName     | test Company          |
    И Я нажимаю на выпадающий список "#fos_user_registration_form_clientType" и выбираю "Коорпоративный клиент"
    И я добавляю в дирректорию"fos_user_registration_form_avatarFile" файл "behat_avatar1.png"
    И я нажимаю на кнопку "Зарегистрироваться"
    И я вижу слово "Поздравляем" на странице

  @login
  Сценарий: Я пытаюсь авторизоваться
    Допустим я нахожусь на главной странице
    И я кликаю по ссылке с id "app_login"
    Тогда я заполняю поля данными
      | json-username | morty@gmail.com |
      | json-password | qwerty          |
    И я нажимаю на кнопку "Войти"
    И Я жду "5" секунд"
    И я вижу слово "Выйти" на странице