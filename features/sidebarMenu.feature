# language: ru

@sidebar @fixtures
Функционал: Тестируем меню навигации Sidebar

  @frontEnd
  Сценарий: Перехожу в раздел новостей для отображения SideBarMenu
    Допустим я перехожу на страницу "app-dynamic-page" с параметром "route" и значением "news"

  @frontEnd
  Структура сценария: Проход по разделам сайта по меню навигации SideBar
    Допустим Я нажимаю на элемент "<element_name>"
    И Я вижу cлово "<page_title>" в элементе "<page_title_element>"
    Тогда Я вижу cлово "<navigation_title>" в элементе ".sidebar-menu-title"

    Примеры:
      | element_name                 | page_title        | navigation_title | page_title_element  |
      | #sidebar-news                | Новости           | Разделы          | h1                  |
      | .module-page-title           | Разделы           | Разделы          | .sidebar-menu-title |
      | #sidebar-review              | Отзывы            | Разделы          | h1                  |
      | #sidebar-coach               | Наши тренеры      | Разделы          | h1                  |
      | .module-page-more            | Разделы           | Разделы          | .sidebar-menu-title |
      | #sidebar-about-us            | О нас             | О нас            | h1                  |
      | #sidebar-our-team            | Наша команда      | О нас            | h1                  |
      | #sidebar-partners            | Партнеры          | О нас            | h1                  |
      | #menu-for-company            | Для компании      | Для компании     | h1                  |
      | #sidebar-company-development | Развитие компаний | Для компании     | h1                  |
      | #sidebar-coaching            | Коучинг           | Коучинг          | h1                  |
      | #sidebar-business-coaching   | Бизнес-коучинг    | Коучинг          | h1                  |
      | #sidebar-life-coaching       | Лайф-коучинг      | Коучинг          | h1                  |
      | #menu-events                 | Мероприятия       | Категории        | h1                  |
      | #sidebar-1                   | Категория pro     | Категории        | h1                  |
      | #menu-material               | Материалы         | Разделы          | h1                  |
      | #sidebar-gallery             | Фотогалерея       | Разделы          | h1                  |
      | #sidebar-contacts            | Контакты          | Разделы          | h1                  |



