# language: ru

@breadcamps @fixtures
Функционал: Тестируем хлебные крошки на страницах

  @frontEnd
  Сценарий: Перехожу в раздел новостей для отображения SideBarMenu
    Допустим я перехожу на страницу "app_single_news" с параметром "id" и значением "1"
    И Я вижу cлово "Друзья!" в элементе "#breadcamp-active-page"
    Тогда Я нажимаю на элемент "#breadcamp-news"
    И Я вижу cлово "Новости" в элементе "#breadcamp-active-page"
    И Я нажимаю на элемент "#breadcamp-homepage"
    Тогда Я вижу cлово "О компании" в элементе "h3"
    И я перехожу на страницу "app_coach_single" с параметром "id" и значением "1"
    И Я вижу cлово "Елена" в элементе "#breadcamp-active-page"
    Тогда Я нажимаю на элемент "#breadcamp-coach"
    И Я вижу cлово "Наши тренеры" в элементе "#breadcamp-active-page"
    И я перехожу на страницу "app-dynamic-page" с параметром "route" и значением "review"
    И Я вижу cлово "Отзывы" в элементе "#breadcamp-active-page"
    И я перехожу на страницу "app-dynamic-page" с параметром "route" и значением "for-company/coaching/life-coaching"
    И Я вижу cлово "Лайф-коучинг" в элементе "#breadcamp-active-page"
    Тогда Я нажимаю на элемент "#breadcamp-coaching"
    И Я вижу cлово "Коучинг" в элементе "#breadcamp-active-page"
    Тогда Я нажимаю на элемент "#breadcamp-for-company"
    И Я вижу cлово "Для компании" в элементе "#breadcamp-active-page"
    И Я нажимаю на элемент "#breadcamp-homepage"
    Тогда Я вижу cлово "О компании" в элементе "h3"





