<?php

namespace App\Repository;

use App\Entity\GalleryImage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method GalleryImage|null find($id, $lockMode = null, $lockVersion = null)
 * @method GalleryImage|null findOneBy(array $criteria, array $orderBy = null)
 * @method GalleryImage[]    findAll()
 * @method GalleryImage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GalleryImageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, GalleryImage::class);
    }

    /**
     * @param $galleryId
     * @return GalleryImage[] Returns an array of GalleryImage objects
     */

    public function findWithMaxPosition($galleryId)
    {
        try {
            return $this->createQueryBuilder('g')
                ->where('g.gallery = :gal')
                ->setParameter('gal', $galleryId)
                ->orderBy('g.position', 'DESC')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        }
    }

    public function findByGalleryId($gallery_id)
    {

        try {
            return $this->createQueryBuilder('t')
                ->andWhere('t.gallery = :gallery_id')
                ->setParameter('gallery_id', $gallery_id)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }

    }

    /*
    public function findOneBySomeField($value): ?GalleryImage
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
