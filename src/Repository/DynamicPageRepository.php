<?php

namespace App\Repository;


use App\Entity\DynamicPage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DynamicPage|null find($id, $lockMode = null, $lockVersion = null)
 * @method DynamicPage|null findOneBy(array $criteria, array $orderBy = null)
 * @method DynamicPage[]    findAll()
 * @method DynamicPage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DynamicPageRepository extends ServiceEntityRepository
{
    public function __construct(
        RegistryInterface $registry
    )
    {
        parent::__construct($registry, DynamicPage::class);
    }


    public function getRouteByArrayRoute(): ?array
    {

        return $this->createQueryBuilder('d')
            ->select('d.pageModule')
            ->where('d.pageModule IS NOT NULL')
            //->orWhere('d.pageModule =: module')
            //->setParameter('module', "null")
            ->getQuery()
            ->getResult();

    }

    public function getDynamicPageByRoute($route): ?DynamicPage
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.route = :route')
                ->setParameter('route', $route)
                ->andWhere('a.isAvailable = :isAvailable')
                ->setParameter('isAvailable', 1)
                ->orderBy('a.left', 'asc')
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function getDynamicPageByModuleName(string $module): ?DynamicPage
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('partial a.{id, title, route, parent, level}')
                ->where('a.pageModule = :pageModule')
                ->setParameter('pageModule', $module)
                ->andWhere('a.isAvailable = :isAvailable')
                ->setParameter('isAvailable', 1)
                ->orderBy('a.left', 'asc')
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function getRootNavigationLinks()
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a.title, a.route, a.id, a.pageModule')
                ->where('a.isShowOnMenu = :isShowOnMenu')
                ->setParameter('isShowOnMenu', 1)
                ->andWhere('a.isAvailable = :isAvailable')
                ->setParameter('isAvailable', 1)
                ->andWhere('a.level  = :level')
                ->setParameter('level', 1)
                ->orderBy('a.left', 'asc')
                ->getQuery()
                ->getResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }

    }

    public function getChildLinksByParent($id)
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a.title, a.route, a.id')
                ->where('a.parent = :parent')
                ->setParameter('parent', $id)
                ->andWhere('a.isShowOnMenu = :isShowOnMenu')
                ->setParameter('isShowOnMenu', 1)
                ->andWhere('a.isAvailable = :isAvailable')
                ->setParameter('isAvailable', 1)
                ->orderBy('a.left', 'asc')
                ->getQuery()
                ->getResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }

    }

    public function getNavigationLinksByPage(DynamicPage $page)
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a.title, a.route, a.id')
                ->where('a.parent  = :parent')
                ->setParameter('parent', $page)
                ->orderBy('a.left', 'asc')
                ->getQuery()
                ->getResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }

    }

    public function getPartialDynamicPageByRoute($route): ?DynamicPage
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('partial a.{id, title, route, parent, level}')
                ->where('a.route = :route')
                ->setParameter('route', $route)
                ->andWhere('a.isAvailable = :isAvailable')
                ->setParameter('isAvailable', 1)
                ->orderBy('a.left', 'asc')
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function searchByWord(?string $word)
    {
        return $this->createQueryBuilder('dp')
            ->select('dp')
            ->orWhere('dp.title LIKE :word')
            ->orWhere('dp.content LIKE :word')
            ->andWhere('dp.isAvailable = 1')
            ->setParameter('word', '%' . $word . '%')
            ->getQuery()
            ->getResult();
    }




}