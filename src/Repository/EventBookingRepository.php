<?php

namespace App\Repository;


use App\Entity\EventBooking;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EventBooking|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventBooking|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventBooking[]    findAll()
 * @method EventBooking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventBookingRepository extends ServiceEntityRepository
{
    public function __construct(
        RegistryInterface $registry
    )
    {
        parent::__construct($registry, EventBooking::class);
    }

    public function getBookingByEvent($user, $event)
    {
        try {
            return $this->createQueryBuilder('e')
                ->select('e')
                ->where('e.user = :user')
                ->andWhere('e.event = :event')
                ->setParameter('user', $user)
                ->setParameter('event', $event)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }


    public function getEventPaymentBySession($session, $user)
    {


        try {
            return $this->createQueryBuilder('b')
                ->select('b')
                ->where('b.user  = :user')
                ->andWhere('b.event = :session')
                ->setParameter('user', $user)
                ->setParameter('session', $session)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }


    }


    public function isAlreadyBooked($session, $user)
    {
        try {
            return $this->createQueryBuilder('b')
                ->select('b.id')
                ->where('b.user  = :user')
                ->andWhere('b.event = :session')
                ->setParameter('user', $user)
                ->setParameter('session', $session)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}