<?php

namespace App\Controller;

use App\Entity\SubscriptionModule;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SubscriptionModuleController extends Controller
{
    /**
     * @Route("subscription", name="app_subscription")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addSubscriptionAction(Request $request)
    {
        $email = $request->request->get('email');
        $subscription = new SubscriptionModule();
        $em = $this->getDoctrine()->getManager();

        $subscription->setEmail($email);
        $subscription->setDate(new \DateTime('now'));

        $em->persist($subscription);
        $em->flush();

        $this->addFlash('notice', 'Вы успешно добавили свой Email !');

        return $this->redirect($request->headers->get('referer'));
    }
}
