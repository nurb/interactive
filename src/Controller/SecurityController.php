<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 25.08.18
 * Time: 20:37
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class SecurityController extends AbstractController
{

    /**
     * @Route("/jlogin", name="login")
     */
    public function login()
    {
        return new JsonResponse(['status' => 'ok']);
    }
}