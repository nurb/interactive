<?php

namespace App\Controller;


use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sonata\AdminBundle\Controller\CRUDController;


class SiteSettingsAdminController extends CRUDController
{
    public function listAction()
    {
        return new RedirectResponse($this->admin->generateObjectUrl('edit', $this->getDoctrine()->getRepository('App:SiteSettings')->find(1)));
    }
}
