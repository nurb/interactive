<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 06.08.18
 * Time: 18:24
 */

namespace App\Controller;


use App\Repository\ContactsModuleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ContactsModuleController extends Controller
{

    /**
     * @Route("/contacts", name="app_review_all")
     * @param ContactsModuleRepository $contactsModuleRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAllContactsAction(ContactsModuleRepository $contactsModuleRepository)
    {
      $arrayLatLngAndAddress = [];

      $contacts = $contactsModuleRepository->findAll();

      foreach ($contacts as $coordinatesAndAdress){
          $arrayLatLngAndAddress[] =  [
             'address' => $coordinatesAndAdress->getAddress(),
             'coordinates' => $coordinatesAndAdress->getCoordinate()
         ];
      }

//      dump($arrayLatLngAndAddress);

      return $this->render('contacts_module/contacts_page.html.twig',[
          'contacts' => $contacts,
          'latlngAndAdress' => $arrayLatLngAndAddress
      ]);
    }
}