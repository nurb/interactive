<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 13.08.18
 * Time: 19:32
 */

namespace App\Controller;


use App\Entity\SiteSettings;
use App\Form\AvatarType;
use App\Form\ProfileEditType;
use App\Repository\EventBookingRepository;
use App\Repository\EventFileRepository;
use App\Repository\EventModuleRepository;
use App\Repository\EventSessionRepository;
use App\Repository\SiteSettingsRepository;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Controller\ChangePasswordController;
use FOS\UserBundle\Form\Type\ChangePasswordFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends Controller
{

    /**
     * @Route("profile", name="app_profile")
     * @param Request $request
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profileAction(
        Request $request,
        ObjectManager $manager
    )
    {

        $user = $this->getUser();
        $form = $this->createForm(AvatarType::class);
        $form->handleRequest($request);

        $data = $form->getData();

        if ($form->isSubmitted() and $form->isValid()) {
            unlink('uploads/images/avatars/' . $user->getAvatar());
            $user->setAvatarFile($data['avatar']);
            $this->get('vich_uploader.upload_handler');
        }

        $manager->persist($user);
        $manager->flush();

        return $this->render('profile/profile_user.html.twig', [
            'user' => $user,
            'avatarForm' => $form->createView()
        ]);

    }

    /**
     * @Route("profile/events", name="app_my_events")
     * @param EventModuleRepository $eventModuleRepository
     * @param EventBookingRepository $bookingRepository
     * @param EventFileRepository $eventFileRepository
     * @param EventSessionRepository $eventSessionRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getEventsForProfile(
        EventModuleRepository $eventModuleRepository,
        EventBookingRepository $bookingRepository,
        EventFileRepository $eventFileRepository,
        EventSessionRepository $eventSessionRepository
    )
    {
        $ArraySessions = [];
        $eventsArray = [];
        $bookings = [];
        $user = $this->getUser();
        $events = $eventModuleRepository->getEventsByUser($user->getId());


        foreach ($events as $event) {
            $eventsArray[] = $event;
            $ArraySessions[] = $event->getEventSessions();
        }

        foreach ($ArraySessions as $sessions) {
            foreach ($sessions as $session) {
                $bookings[] = [
                    'booking' => $bookingRepository->getEventPaymentBySession($session, $user),
                    'session' => $session
                ];
            }
        }
        return $this->render('profile/profile_events_user.html.twig', [
            'eventsArray' => $eventsArray,
            'bookings' => $bookings
        ]);
    }

    /**
     * @Route("events/remove/{id}", name="app_remove_booking", requirements={"id": "\d+"})
     * @Method({"GET","HEAD"})
     * @param int $id
     * @param EventBookingRepository $bookingRepository
     * @param EventModuleRepository $eventModuleRepository
     * @param SiteSettingsRepository $settingsRepository
     * @param \Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function removeEvent(
        int $id,
        EventBookingRepository $bookingRepository,
        EventModuleRepository $eventModuleRepository,
        SiteSettingsRepository $settingsRepository,
        \Swift_Mailer $mailer
    )
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $event = $eventModuleRepository->find($id);

        $eventBooking = $bookingRepository->getEventPaymentBySession($event, $user);

        $event->setOrderLimit($event->getOrderLimit()+1);


        
        $settings = $settingsRepository->getEmails();
        $mails = explode(',',$settings->getAdminEmail());

        foreach($mails as $mail){
            $session = new Session();
            $message = new \Swift_Message('Отмена бронирования');
            $message->setFrom($session->get('adminEmail'));

            $message->setTo($mail);

            $message->setBody(
                $this->renderView(
                // templates/emails/registration.html.twig
                    'events_module/booking_messages/send_remove.html.twig',[
                        'user' => $this->getUser(),
                        'booking' => $eventBooking,
                        'home_url' =>  'http://'.$_SERVER['HTTP_HOST'].'/admin',
                        'send_event_url' => 'http://'.$_SERVER['HTTP_HOST'].'/events/'.$event->getId(),
                    ]
                ),
                'text/html'
            );

            $mailer->send($message);
        }



        foreach($event->getEventSessions() as $session){
            $eventBooking = $bookingRepository->getEventPaymentBySession($session, $user);
            $em->remove($eventBooking);
            $em->flush();
        }


        return $this->redirectToRoute('app_my_events');

    }

    /**
     * @Route("profile/edit", name="app_edit_profile")
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editProfile(Request $request)
    {
        $user = $this->getUser();

        $form = $this->createForm(ProfileEditType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('app_profile');
        }

        return $this->render('profile/edit_profile.html.twig', [
            'form' => $form->createView()
        ]);
    }
}