<?php


namespace App\Controller;


use App\Repository\ReviewRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ReviewModuleController extends Controller
{
    /**
     * @Route("/reviews/slider", name="app_review_slider")
     * @param ReviewRepository $reviews
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function  showReviewSliderAction(ReviewRepository $reviews){

        $reviews = $reviews->getIsOnMainPage();

        return $this->render('review_module/slider_review.html.twig',[
            'reviews' => $reviews
        ]);
    }
    /**
     * @Route("/reviews", name="app_review_all")
     * @param ReviewRepository $reviewRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAllReviewAction(ReviewRepository $reviewRepository)
    {
        $reviews = $reviewRepository->findAll();

        return $this->render('review_module/page_review.html.twig', [
            'reviews' => $reviews
        ]);
    }

}