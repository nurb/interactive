<?php


namespace App\Controller;


use App\Repository\DynamicPageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;


class DynamicPagesController extends Controller
{
    /**
     * @Route("/page/{route}", name="app-dynamic-page", requirements={"route"=".+"})
     * @param string $route
     * @param DynamicPageRepository $dynamicPageRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getPageAction(string $route, DynamicPageRepository $dynamicPageRepository)
    {
        //Если роут окочанчивается на "/", то обрезаем роут, например: forCompany/companyDevelopment/teamBuilding/
        if (substr($route, -1) === "/") {
            $route = substr($route, 0, -1);
        }

        $rawRoute = $route;

        //Получаем последнию дирикторию роута
        if (strpos($route, '/') !== false) {
            $routeArray = explode("/", $route);
            $route = end($routeArray);
        }


        $page = $dynamicPageRepository->getDynamicPageByRoute($route);

        //Если сущность не найдена по роуту или не совпдает путь роута, ты кидаем исключение 404
        if ($page == null || $rawRoute !== $page->getFullRoute()) {
            throw $this->createNotFoundException();
        }


        return $this->render('dynamic_page/page.html.twig', [
            'page' => $page,
        ]);
    }
}