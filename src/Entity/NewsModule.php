<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NewsModuleRepository")
 * @Vich\Uploadable
 */
class NewsModule
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string" , length=1024)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $image;

    /**
     * @Assert\File(
     *     mimeTypes = { "image/gif", "image/jpeg", "image/png" },
     *     mimeTypesMessage = "Недопустимый тип данных ({{ type }}). Допустимы: {{ types }}.",
     *     maxSize = "2000k",
     *     maxSizeMessage = "Размер файла привышает 2 мегабайта"
     * )
     * @Assert\Image(
     *     minRatio = "0.5",
     *     maxRatio = "2",
     *     minHeight="20",
     *     maxHeight = "1080",
     *     minWidth="20",
     *     maxWidth = "1080",
     *     maxRatioMessage="Неправильные пропорции изображения",
     *     minRatioMessage="Неправильные пропорции изображения",
     *     minHeightMessage="Мминимально допустимая высота 20px",
     *     maxHeightMessage="Максимально допустимая высота 1080px",
     *     minHeightMessage="Максимально допустимая ширина 20px",
     *     maxHeightMessage="Максимально допустимая ширина 1080ppx",
     * )
     * @Vich\UploadableField(mapping="news_photo", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=1024)
     * @var string
     */
    private $shortDescription;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $fullDescription;

    public function __construct()
    {
        $this->date = new \DateTime('now');
    }

    /**
     * @param string $title
     * @return NewsModule
     */
    public function setTitle(string $title): NewsModule
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }


    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param File $imageFile
     * @return NewsModule
     */
    public function setImageFile(File $imageFile): NewsModule
    {
        $this->imageFile = $imageFile;
        return $this;
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param \DateTime $date
     * @return NewsModule
     */
    public function setDate(\DateTime $date): NewsModule
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $shortDescription
     * @return NewsModule
     */
    public function setShortDescription(string $shortDescription): NewsModule
    {
        $this->shortDescription = $shortDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * @param string $fullDescription
     * @return NewsModule
     */
    public function setFullDescription(string $fullDescription): NewsModule
    {
        $this->fullDescription = $fullDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullDescription()
    {
        return $this->fullDescription;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}