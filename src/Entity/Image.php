<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 * @Vich\Uploadable
 */
class Image
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @Assert\File(
     *     mimeTypes = { "image/gif", "image/jpeg", "image/png" },
     *     mimeTypesMessage = "Недопустимый тип данных ({{ type }}). Допустимы: {{ types }}.",
     *     maxSize = "2000k",
     *     maxSizeMessage = "Размер файла привышает 2 мегабайта"
     * )
     * @Assert\Image(
     *     minRatio = "0.5",
     *     maxRatio = "2",
     *     minHeight="20",
     *     maxHeight = "1280",
     *     minWidth="20",
     *     maxWidth = "1280",
     *     maxRatioMessage="Неправильные пропорции изображения",
     *     minRatioMessage="Неправильные пропорции изображения",
     *     minHeightMessage="Мминимально допустимая высота 20px",
     *     maxHeightMessage="Максимально допустимая высота 1280px",
     *     minHeightMessage="Максимально допустимая ширина 20px",
     *     maxHeightMessage="Максимально допустимая ширина 1280ppx",
     * )
     * @Vich\UploadableField(mapping="images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\GalleryImage", inversedBy="image", cascade={"persist", "remove"})
     */
    private $galleryImage;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return Image
     */
    public function setImage(string $image = null): ?self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @param File $imageFile
     *
     * @return Image
     */
    public function setImageFile(File $imageFile)
    {
        $this->imageFile = $imageFile;
        return $this;
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @return GalleryImage|null
     */
    public function getGalleryImage(): ?GalleryImage
    {
        return $this->galleryImage;
    }

    /**
     * @param GalleryImage|null $galleryImage
     * @return Image
     */
    public function setGalleryImage(?GalleryImage $galleryImage): self
    {
        $this->galleryImage = $galleryImage;

        return $this;
    }
}
