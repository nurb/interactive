<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CoachModuleRepository")
 * @Vich\Uploadable
 */
class CoachModule
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fullName;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $image;

    /**
     * @Assert\File(
     *     mimeTypes = { "image/gif", "image/jpeg", "image/png" },
     *     mimeTypesMessage = "Недопустимый тип данных ({{ type }}). Допустимы: {{ types }}.",
     *     maxSize = "2000k",
     *     maxSizeMessage = "Размер файла привышает 2 мегабайта"
     * )
     * @Assert\Image(
     *     minRatio = "0.5",
     *     maxRatio = "2",
     *     minHeight="20",
     *     maxHeight = "1080",
     *     minWidth="20",
     *     maxWidth = "1080",
     *     maxRatioMessage="Неправильные пропорции изображения",
     *     minRatioMessage="Неправильные пропорции изображения",
     *     minHeightMessage="Мминимально допустимая высота 20px",
     *     maxHeightMessage="Максимально допустимая высота 1080px",
     *     minHeightMessage="Максимально допустимая ширина 20px",
     *     maxHeightMessage="Максимально допустимая ширина 1080ppx",
     * )
     * @Vich\UploadableField(mapping="coach_photo", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=1025)
     */
    private $shortDescription;

    /**
     * @ORM\Column(type="text")
     */
    private $fullDescription;

    /**
     * @ORM\Column(type="integer")
     */
    private $priority;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     *
     */
    private $isActive;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\EventModule", inversedBy="eventCoachs")
     */
    private $events;

    public function __construct()
    {
        $this->events = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }


    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    public function setShortDescription(string $shortDescription): self
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    public function getFullDescription(): ?string
    {
        return $this->fullDescription;
    }

    public function setFullDescription(string $fullDescription): self
    {
        $this->fullDescription = $fullDescription;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @param bool $isActive
     * @return CoachModule
     */
    public function setIsActive(bool $isActive): CoachModule
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @return ArrayCollection
     */
    public function getEvents(): ArrayCollection
    {
        return $this->events;
    }

    public function addEvent(EventModule $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->addEventCoach($this);
        }

        return $this;
    }

    public function removeEvent(EventModule $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            $event->removeEventCoach($this);
        }

        return $this;
    }


    public function __toString()
    {
        return $this->fullName??'';
    }
}
