<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 27.08.18
 * Time: 19:21
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\SiteSettingsRepository")
 */
class SiteSettings
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string" , length=128)
     */
    private $mainPageTitle;

    /**
     * @var string
     *
     * @ORM\Column(type="string" , length=1024)
     */
    private $mainPageDescription;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $mainSliderTimer;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $coachSliderTimer;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $reviewSliderTimer;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $videoGallerySliderTimer;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $eventsSliderTimer;

    /**
     * @var string
     *
     * @ORM\Column(type="text", length=4096)
     */
    private $mainPageTextBlock;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1024)
     */
    private $adminEmail;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1024)
     */
    private $facebookUrl;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1024)
     */
    private $instagramUrl;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1024)
     */
    private $vkontakteUrl;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private $twitterUrl;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $mainPageTitle
     * @return SiteSettings
     */
    public function setMainPageTitle(string $mainPageTitle): SiteSettings
    {
        $this->mainPageTitle = $mainPageTitle;
        return $this;
    }

    /**
     * @return string
     */
    public function getMainPageTitle()
    {
        return $this->mainPageTitle;
    }

    /**
     * @param string $mainPageDescription
     * @return SiteSettings
     */
    public function setMainPageDescription(string $mainPageDescription): SiteSettings
    {
        $this->mainPageDescription = $mainPageDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getMainPageDescription()
    {
        return $this->mainPageDescription;
    }

    /**
     * @param int $mainSliderTimer
     * @return SiteSettings
     */
    public function setMainSliderTimer(int $mainSliderTimer): SiteSettings
    {
        $this->mainSliderTimer = $mainSliderTimer;
        return $this;
    }

    /**
     * @return int
     */
    public function getMainSliderTimer()
    {
        return $this->mainSliderTimer;
    }

    /**
     * @param int $coachSliderTimer
     * @return SiteSettings
     */
    public function setCoachSliderTimer(int $coachSliderTimer): SiteSettings
    {
        $this->coachSliderTimer = $coachSliderTimer;
        return $this;
    }

    /**
     * @return int
     */
    public function getCoachSliderTimer()
    {
        return $this->coachSliderTimer;
    }

    /**
     * @param int $reviewSliderTimer
     * @return SiteSettings
     */
    public function setReviewSliderTimer(int $reviewSliderTimer): SiteSettings
    {
        $this->reviewSliderTimer = $reviewSliderTimer;
        return $this;
    }

    /**
     * @return int
     */
    public function getReviewSliderTimer()
    {
        return $this->reviewSliderTimer;
    }

    /**
     * @param int $eventsSliderTimer
     * @return SiteSettings
     */
    public function setEventsSliderTimer(int $eventsSliderTimer): SiteSettings
    {
        $this->eventsSliderTimer = $eventsSliderTimer;
        return $this;
    }

    /**
     * @return int
     */
    public function getEventsSliderTimer()
    {
        return $this->eventsSliderTimer;
    }

    /**
     * @param string $mainPageTextBlock
     * @return SiteSettings
     */
    public function setMainPageTextBlock(string $mainPageTextBlock): SiteSettings
    {
        $this->mainPageTextBlock = $mainPageTextBlock;
        return $this;
    }

    /**
     * @return string
     */
    public function getMainPageTextBlock()
    {
        return $this->mainPageTextBlock;
    }

    /**
     * @param string $adminEmail
     * @return SiteSettings
     */
    public function setAdminEmail(string $adminEmail): SiteSettings
    {
        $this->adminEmail = $adminEmail;
        return $this;
    }

    /**
     * @return string
     */
    public function getAdminEmail(): string
    {
        return $this->adminEmail;
    }

    /**
     * @param string $facebookUrl
     * @return SiteSettings
     */
    public function setFacebookUrl(string $facebookUrl): SiteSettings
    {
        $this->facebookUrl = $facebookUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getFacebookUrl(): string
    {
        return $this->facebookUrl;
    }

    /**
     * @param string $instagramUrl
     * @return SiteSettings
     */
    public function setInstagramUrl(string $instagramUrl): SiteSettings
    {
        $this->instagramUrl = $instagramUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getInstagramUrl(): string
    {
        return $this->instagramUrl;
    }

    /**
     * @param int $videoGallerySliderTimer
     * @return SiteSettings
     */
    public function setVideoGallerySliderTimer(int $videoGallerySliderTimer): SiteSettings
    {
        $this->videoGallerySliderTimer = $videoGallerySliderTimer;
        return $this;
    }

    /**
     * @return int
     */
    public function getVideoGallerySliderTimer(): int
    {
        return $this->videoGallerySliderTimer;
    }

    /**
     * @param string $vkontakteUrl
     * @return SiteSettings
     */
    public function setVkontakteUrl(string $vkontakteUrl): SiteSettings
    {
        $this->vkontakteUrl = $vkontakteUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getVkontakteUrl(): string
    {
        return $this->vkontakteUrl;
    }

    public function getTwitterUrl(): ?string
    {
        return $this->twitterUrl;
    }

    public function setTwitterUrl(?string $twitterUrl): self
    {
        $this->twitterUrl = $twitterUrl;

        return $this;
    }


}