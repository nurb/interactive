<?php

namespace App\Form;



use App\Model\Enumeration\ClientTypeEnumeration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ProfileEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('phoneNumber',TextType::class,[
                'label' => 'Телефон'
            ])
            ->add('fullName',TextType::class,[
                'label' => 'Ф.И.О'
            ])
            ->add('organizationName',TextType::class,[
                'label' => 'Название организации'
            ])
            ->add('clientType', ChoiceType::class, array(
                'label' => 'Кто Вы?',
                'choices' => [
                    'Коорпоративный клиент' => ClientTypeEnumeration::CORPORATE_CLIENT,
                    'Частное лицо' => ClientTypeEnumeration::PRIVATE_CLIENT],
                'placeholder' => 'Выбери один из вариантов'
            ))
            ->add('Редактировать',SubmitType::class);
        parent::buildForm($builder, $options);
        $builder->remove('current_password');

    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\ProfileFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_profile_edit';
    }
}
