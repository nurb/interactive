<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Model\Enumeration\ClientTypeEnumeration;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{

    /**
     * @param ObjectManager $manager
     */
    public function load
    (
        ObjectManager $manager
    )
    {
        $admin = new User();
        $admin
            ->setEmail('interactivecompany.kg@gmail.com')
            ->setPhoneNumber('+996 556 59 92 79')
            ->setPlainPassword('LbefoSow6nrF3vLq5aMY')
            ->setEnabled(true)
            ->setRoles(['ROLE_ADMIN'])
            ->setClientType(ClientTypeEnumeration::PRIVATE_CLIENT)
            ->setFullName('Администратор')
            ->setOrganizationName('ОсОО Интерактив')
            ->setAvatar('avatar2.png');
        $manager->persist($admin);
        $this->addReference('admin', $admin);
        $manager->flush();

       $user = new User();
       $user
           ->setEmail('morty@gmail.com')
           ->setPhoneNumber('+996 555 55 55 55')
           ->setPlainPassword('qwerty')
           ->setEnabled(true)
           ->setRoles(['ROLE_USER'])
           ->setClientType(ClientTypeEnumeration::PRIVATE_CLIENT)
           ->setFullName('Иванов Иван Иванович')
           ->setOrganizationName('ОсОО Интерактив')
           ->setAvatar('avatar1.png');

       $manager->persist($user);
       $this->addReference('user', $user);
       $manager->flush();


    }
}
