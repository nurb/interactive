<?php

namespace App\DataFixtures;


use App\Entity\SiteSettings;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class SiteSettingsFixtures extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $siteSettings = new SiteSettings();
        $siteSettings
            ->setMainPageTitle('Главная')
            ->setMainPageDescription('Деятельность по консультированию руководителей, управленцев по широкому кругу 
            вопросов в сфере финансовой, коммерческой, юридической, технологической, технической, экспертной 
            деятельности. Цель консалтинга — помочь системе управления Деятельность по консультированию руководителей, 
            управленцев по широкому кругу вопросов в сфере финансовой, коммерческой, юридической, технологической, 
            технической, экспертной деятельности. Цель консалтинга — помочь системе управления')
            ->setMainPageTextBlock('<h3>О компании</h3>
                <p><span class="purple">Консалтинговый центр «ИнтерАктив»</span></p>
                <p>Специализация: Управленческий консалтинг, корпоративное обучение, командопостроение, коучинг.</p>
                <p><span class="purple">Кто мы?</span></p>
                <p>Консалтинговый центр «ИнтерАктив» основан на базе учреждения «Центр ПсихоТехнологий» в 2011 году.
                    (Учреждение «Центр ПсихоТехнологий» успешно функционирует на рынке Кыргызстана с 2005 года.)</p>
                <p>
                    <span class="purple">Учредители Консалтингового Центра «ИнтерАктив»: Михаил Мунькин и Елена Бевзова.</span>
                </p>
                <p>Консалтинговый центр “ИнтерАктив” в настоящий момент ведет свою деятельность в двух основных
                    областях: Психология и Бизнес. Мы специализируемся в сфере организационного консалтинга, оценки и
                    обучения персонала. А также занимаемся оказанием психологических и психотерапевтических услуг,
                    образованием в сфере психологии, ведением проектов социально-психологической направленности при
                    поддержке международных организаций. </p>')
            ->setMainSliderTimer(5000)
            ->setCoachSliderTimer(5000)
            ->setEventsSliderTimer(5000)
            ->setVideoGallerySliderTimer(5000)
            ->setAdminEmail('interactivecompany.kg@gmail.com')
            ->setFacebookUrl('https://www.facebook.com/interactive.kg')
            ->setInstagramUrl('https://www.instagram.com/interactive.kg')
            ->setVkontakteUrl('https://vk.com/interactivekg')
            ->setTwitterUrl('https://twitter.com/interactive_kg')
            ;
        $manager->persist($siteSettings);
        $manager->flush();
    }

}