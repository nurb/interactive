<?php

namespace App\DataFixtures;


use App\Entity\VideoGalleryModule;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class VideoGalleryModuleFixtures extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $names = ['НЛП в Бишкеке. Обучение на курсе НЛП-Практик в КЦ "ИнтерАктив"',
            'Тренинг "Карьерный рост"',
            'Михаил Мунькин. Тренинг "Победить в переговорах"!',
            'Компания Интерактив и Аркадий Бондарь проводят сертификацию НЛПеров в Бишкеке! Начало...'];

        $links = ["PJ1KVid6vSw",
            "G27E8cRE6Oc",
            "xN9XjHRh2PM",
            "orJmQAaGLFs"];


        for ($i = 0; $i < 4; $i++) {
            $videoGalleryModule = new VideoGalleryModule();
            $videoGalleryModule
                ->setName($names[$i])
                ->setLink($links[$i])
                ->setIsActive(1);

            $manager->persist($videoGalleryModule);
        }
        $manager->flush();
    }

}