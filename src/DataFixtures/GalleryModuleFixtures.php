<?php

namespace App\DataFixtures;


use App\Entity\GalleryImage;
use App\Entity\GalleryModule;
use App\Entity\Image;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


class GalleryModuleFixtures extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $images = ['gallery1.png', 'gallery2.png', 'gallery3.png','gallery4.png'];
        $shortDescriptions = 'В 2015 г. состоялась очередная сертификация с присвоением квалификации "НЛП-Практик" совместно с Институтом НЛП, г. Санкт-Петербург. Наших специалистов в области НЛП-технологий становится все больше и это хорошая новость!.';
        $albumName = 'Сертификация НЛП-Практиков 2015';
        $galleryModule = new GalleryModule();
        $galleryModule->setShortDescription($shortDescriptions);
        $galleryModule->setAlbumName($albumName);
        $galleryModule->setData(new \DateTime('now'));
        $galleryModule->setIsActive(true);
        $galleryModule->setPriority(1);
        for($i = 0; $i < 4; $i++){
            $galleryImage = new GalleryImage();
            $galleryImage->setPosition($i);
            $galleryImage->setGallery($galleryModule);
            $image = new Image();
            $image
                ->setImage($images[$i])
                ->setGalleryImage($galleryImage);
            $galleryModule->addImage($galleryImage);
            $manager->persist($image);
            $manager->persist($galleryImage);
        }
        $manager->persist($galleryModule);
        $manager->flush();
    }
}