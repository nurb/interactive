<?php

namespace App\DataFixtures;


use App\Entity\ReviewModule;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ReviewModuleFixtures extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $names = ['Рассказ участника курса НЛП-Практик в центре "ИнтерАктив"',
            'Отзыв участницы о курсе НЛП',
            'отзыв о курсе "НЛП-Практик" от компании "ИнтерАктив"'];

        $links = ["vCvuVWK-3Rk",
            "-GVw3-P1HqU",
            "lpJd1VPzgiM"];


        for ($i = 0; $i < 3; $i++) {
            $reviewModule = new ReviewModule();
            $reviewModule
                ->setName($names[$i])
                ->setLink($links[$i])
                ->setIsActive(1);

            $manager->persist($reviewModule);
        }
        $manager->flush();
    }

}