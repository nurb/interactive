<?php

namespace App\DataFixtures;


use App\Entity\EventCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class EventCategoryFixtures extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $eventCategory1 = new EventCategory();
        $eventCategory1
            ->setName('pro');
        $manager->persist($eventCategory1);
        $this->addReference('eventCategory1', $eventCategory1);

        $eventCategory2 = new EventCategory();
        $eventCategory2
            ->setName('business');
        $manager->persist($eventCategory2);
        $this->addReference('eventCategory2', $eventCategory2);

        $eventCategory3 = new EventCategory();
        $eventCategory3
            ->setName('general');
        $manager->persist($eventCategory3);
        $this->addReference('eventCategory3', $eventCategory3);

        $manager->flush();
    }

}