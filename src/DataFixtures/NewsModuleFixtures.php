<?php

namespace App\DataFixtures;


use App\Entity\NewsModule;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Validator\Constraints\Date;

class NewsModuleFixtures extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $titles = ['Друзья! В Феврале 2018 года начнется курс НЛП-Мастер!',
            'Запускается курс по психологии управления! Уже очень скоро!',
            'Ура! Мы обновили сайт!'];
        $images = ['news1.png', 'news2.png', 'news3.jpg'];
        $shortDescriptions = ['Теперь вы можете получить квалификацию НЛП-Мастер на территории Кыргызстана',
            'Отличный курс, проводим его редко в силу загруженности, так что успейте записаться в группу!',
            'Друзья! Мы обновили свой сайт.'];
        $fullDescription = ['<p>Теперь вы можете получить квалификацию НЛП-Мастер на территории Кыргызстана и по ценам, 
        соответствующим нашим реалиям. НЛП-Практики, спешите записаться в группу!</p>
        <p>Все подробности ждут вас в рубрике Обучение! Справки по телефону: 0556 699 279</p>',
            '<p>Отличный курс, проводим его редко в силу загруженности, так что успейте записаться в группу! Справки по телефону: 0556 699 279<p>',
            '<p>Друзья! Мы обновили свой сайт. Теперь работать с нами еще удобнее!</p>'];
        $dates = [new \DateTime('12.12.2017'),
            new \DateTime('03.02.2018'),
            new \DateTime('01.12.2018')];


        for($i = 0; $i < 3; $i++){
            $newsModule = new NewsModule();
            $newsModule
                ->setTitle($titles[$i])
                ->setImage($images[$i])
                ->setShortDescription($shortDescriptions[$i])
                ->setFullDescription($fullDescription[$i])
                ->setDate($dates[$i]);
                $manager->persist($newsModule);
        }
        $manager->flush();
    }

}