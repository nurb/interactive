<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Route\RouteCollection;

class SiteSettingsAdmin extends AbstractAdmin
{
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
    }
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {

    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('mainPageTitle', TextType::class, array('label' => 'Meta заголовок'))
            ->add('mainPageDescription', TextType::class, array('label' => 'Meta Description'))
            ->add('mainPageTextBlock', TextareaType::class, array('label' => 'Текстовый блок на главной', 'attr' => array('class' => 'ckeditor')))
            ->add('mainSliderTimer', NumberType::class, array('label' => 'Таймер основного слайдера'))
            ->add('coachSliderTimer', NumberType::class, array('label' => 'Таймер слайдера тренеров'))
            ->add('eventsSliderTimer', NumberType::class, array('label' => 'Таймер слайдера мероприятий'))
            ->add('adminEmail', TextType::class, array('label' => 'Email администратора'))
            ->add('facebookUrl', TextType::class, array('label' => 'Адрес страницы на facebook'))
            ->add('instagramUrl', TextType::class, array('label' => 'Адрес страницы в instagram'))
            ->add('vkontakteUrl', TextType::class, array('label' => 'Адрес страницы в vk'))
            ->add('twitterUrl', TextType::class, array('label' => 'Адрес страницы в twitter'))
        ;
    }
}