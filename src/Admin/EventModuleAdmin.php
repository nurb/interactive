<?php

namespace App\Admin;

use App\Entity\CoachModule;
use App\Entity\EventSession;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\EventCategory;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class EventModuleAdmin extends AbstractAdmin
{
    public function createQuery($context = 'list')
    {
        $proxyQuery = parent::createQuery('list');
        $proxyQuery->addOrderBy('o.priority', 'ASC');
        return $proxyQuery;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('priorityUp', $this->getRouterIdParameter() . '/priorityUp');
        $collection->add('priorityDown', $this->getRouterIdParameter() . '/priorityDown');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('eventName', null, array('label' => 'Название'))
            ->add('shortDescription', null, array('label' => 'Короткое описание'))
            ->add('fullDescription', null, array('label' => 'Полное описание'))
            ->add('eventStart', null, array('label' => 'Дата начала'))
            ->add('eventEnding', null, array('label' => 'Дата окончания'))
            ->add('priority', null, array('label' => 'Приоритет'))
            ->add('orderLimit', null, array('label' => 'Количество мест'))
            ->add('eventType', null, array('label' => 'Тип'))
            ->add('isActive', null, array('label' => 'Отображение'));
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('eventName', null, array('label' => 'Название', 'route' => ['name' => 'show']))
            ->add('isActive', null, array('label' => 'Отображение', 'editable' => true))
            ->add('eventBookings', 'actions', array(
                'label' => 'Сеансы мероприятия',
                'actions' => array(
                    array('template' => 'SonataAdmin/CRUD/events/show_session.html.twig')
                )
            ))
            ->add('priority', 'actions', array(
                'label' => 'Приоритет',
                'actions' => array(
                    'priorityUp' => array(
                        'template' => 'SonataAdmin/CRUD/couch/up.html.twig'
                    ),
                    'priorityDown' => array(
                        'template' => 'SonataAdmin/CRUD/couch/down.html.twig'
                    )
                )
            ))
            ->add('_action', null, array('label' => 'Действие',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),

                ),
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $manager = $this->getConfigurationPool()
            ->getContainer()->get('doctrine')
            ->getManager()
            ->getRepository('App\Entity\EventModule');
        $position = $manager->findLastElementPosition();
        $position = $position['priority'];
        if ($position == null){
            $position = 1;
        }else{
            $position++;
        }

        $subject = $this->getRoot()->getSubject()->getId();
        if ($subject != null) {
            $isImageRequered = false;
        } else {
            $isImageRequered = true;
        }


        $formMapper
            ->add('eventName', TextType::class, array('label' => 'Название'))
            ->add('imageFile', FileType::class, array('label' => 'Изображение', 'required' => $isImageRequered))
            ->add('shortDescription', null, array('label' => 'Короткое описание'))
            ->add('fullDescription', TextareaType::class, array('label' => 'Полное описание', 'attr' => array('class' => 'ckeditor')))
            ->add('eventStart', DateType::class, array('label' => 'Дата начала', 'widget' => 'single_text'))
            ->add('eventEnding', DateType::class, array('label' => 'Дата окончания', 'widget' => 'single_text'))
            ->add('priority', HiddenType::class, array('data' => $position))
            ->add('orderLimit', null, array('label' => 'Количество мест'))
            ->add('categories', ModelType::class, array('label' => 'Категории','multiple' => true))
            ->add('eventType', ChoiceType::class, array('label' => 'Тип',
                'choices' => array(
                    'Разовое' => 'one-time',
                    'Длительное' => 'recurrent',
                )))
            ->add('eventCoachs', EntityType::class, array('label' => 'Тренеры', 'class' => CoachModule::class, 'multiple' => true))
            ->add('isActive', null, array('label' => 'Отображение'));
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('eventName', null, array('label' => 'Название'))
            ->add('shortDescription', null, array('label' => 'Короткое описание'))
            ->add('eventStart', null, array('label' => 'Дата начала', 'format' => 'd.m.Y'))
            ->add('eventEnding', null, array('label' => 'Дата окончания', 'format' => 'd.m.Y'))
            ->add('orderLimit', null, array('label' => 'Количество мест'))
            ->add('eventType', null, array('label' => 'Тип'))
            ->add('isActive', null, array('label' => 'Отображение'));
    }


}